<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The services file for defining the functions and services for this webservice in the DB
 *
 * @package    audio_ws
 * @copyright  2017 Raymond Mlambo <khozanai@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$services = array(
    'audio_ws_get_audios' => array(//the name of the web service
        'functions' => array('audio_ws_get_audios'), //web service functions of this service
        'requiredcapability' => '', //if set, the web service user need this capability to access 
        //any function of this service. For example: 'some/capability:specified'                 
        'restrictedusers' => 0, //if enabled, the Moodle administrator must link some user to this service
        //into the administration
        'enabled' => 1, //if enabled, the service can be reachable on a default installation
    )
);

$functions = array(
    'audio_ws_get_audios' => array(//web service function name
        'classname' => 'local_audio_ws_external', //class containing the external function
        'methodname' => 'audio_ws_get_audios', //external function name
        'classpath' => 'local/audio_ws/externallib.php', //file containing the class/external function
        'description' => 'Fetch all audio plugins from course', //human readable description of the web service function
        'type' => 'read', //database rights of the web service function (read, write)
    ),
);
