<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main class for the audio web services
 *
 * @package    audio_ws
 * @copyright  2017 Raymond Mlambo
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . "/externallib.php");

class local_audio_ws_external extends external_api {

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function audio_ws_get_audios_parameters() {
        return new external_function_parameters(
                array(
            'courseid' => new external_value(PARAM_INT, 'id of course')
                )
        );
    }

    /**
     * Get Contexts
     * @param array $contexts array of context description arrays (with contextlevel and instanceid)
     * @return array of contexts
     */
    public static function audio_ws_get_audios($courseid) {
        global $DB;
        $params = self::validate_parameters(self::audio_ws_get_audios_parameters(), array(
                    'courseid' => $courseid
        ));
        $params = (object) $params;

        $course = $DB->get_record('course', array('id' => $params->courseid));

        if (empty($course)) {
            throw new invalid_parameter_exception('Invalid course id');
        }

        $coursecontext = context_course::instance($params->courseid);

        $courseid = $params->courseid;
        
        $audios = $DB->get_records('audio', array('course' => $courseid));

        foreach ($audios as $audio) {
            return $audio->id;
        }
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function audio_ws_get_audios_returns() {
        return new external_value(PARAM_FLOAT, 'List of all the audio files in a given course');
    }

}
