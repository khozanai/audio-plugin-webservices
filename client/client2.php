<?php
// This client for local_getcontexts is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//

/**
 * XMLRPC client for Moodle 2 - local_getcontexts
 *
 * This script does not depend of any Moodle code,
 * and it can be called from a browser.
 *
 * @author Jerome Mouneyrac modified by Francois Jacquet
 */

/// MOODLE ADMINISTRATION SETUP STEPS
// 1- Install the plugin
// 2- Enable web service advance feature (Admin > Advanced features)
// 3- Enable XMLRPC protocol (Admin > Plugins > Web services > Manage protocols)
// 4- Create a token for a specific user (Admin > Plugins > Web services > Manage tokens)
// 5- Run this script directly from your browser: you should see 'Hello, FIRSTNAME'

/// SETUP - NEED TO BE CHANGED
$token = '81e1e43d77317da8370915ef2236f227';
$domainname = 'http://localhost/moodle27';

/// FUNCTION NAME
$functionname = 'local_getgrades_set_grades';			
$details = 	array(2, //Course Id
                  3, //User id
				  'assignement1', //grade item idnumber
				  80, //itemgrade
				  'Good work' // feedback
                      );

///// XML-RPC CALL
header('Content-Type: text/plain');
$serverurl = $domainname . '/webservice/xmlrpc/server.php'. '?wstoken=' . $token;
require_once('./curl.php');
$curl = new curl;
$post = xmlrpc_encode_request($functionname, $details);
$resp = xmlrpc_decode($curl->post($serverurl, $post));
print_r($resp);
