<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once('../../../config.php');

global $DB, $CFG;
require_once($CFG->libdir . "/gradelib.php");
require_once($CFG->dirroot . "/grade/querylib.php");


//$userid = 3;
//$courseid = 2;
//$gradeitemidnumber = 'assignement1';
//$gradeitemgrade = 60;
//$feedback = 'Good work keep it up';

$userid = 4;
$courseid = 2;
$gradeitemidnumber = 'assignement1';
$gradeitemgrade = 10;
$feedback = 'Good work student2.We are proud of you';

//$userid = $params->userid;
//$courseid = $params->courseid;
//$gradeitemidnumber = $params->itemidnumber;
//$gradeitemgrade = $params->itemgrade;
$feedback_str = trim($feedback);

if ($feedback_str !== '') {
    $feedback = $feedback_str;
} else {
    $feedback = '';
}
//        $feedback = $feedback;

$gradeitem = $DB->get_record('grade_items', array('idnumber' => $gradeitemidnumber, 'courseid' => $courseid));
if (empty($gradeitem)) {
    throw new invalid_parameter_exception("Grade item with idnumber {$gradeitemidnumber} does not exists");
}


if ($gradeitem->gradetype == 1) {
    if ((!filter_var($gradeitemgrade, FILTER_VALIDATE_FLOAT) or ! filter_var($gradeitemgrade, FILTER_VALIDATE_INT)) && $gradeitemgrade >= 0) {
        throw new invalid_parameter_exception("Only float or integer grade type are accepted on this grade item");
    }
//            if (!is_int($gradeitemgrade) or !is_float($gradeitemgrade) ) {
//                throw new invalid_parameter_exception("Only float or integer grade type are accepted on this grade item");
//            }

    if ($gradeitemgrade > $gradeitem->grademax) {
        throw new invalid_parameter_exception("You can give maximum of $gradeitem->grademax on this grade item or activity");
    }

    if ($gradeitemgrade < $gradeitem->grademin) {
        throw new invalid_parameter_exception("You need to give minimum of $gradeitem->grademin on this gradeitem");
    }
}

if ($gradeitem->itemmodule == 'assign') {
    if ($DB->record_exists('assign_grades', array('assignment' => $gradeitem->iteminstance, 'userid' => $userid))) {
        $assigngrade = $DB->get_record('assign_grades', array('assignment' => $gradeitem->iteminstance, 'userid' => $userid));
        $assignmentgrades = new stdClass();
        $assignmentgrades->id = $assigngrade->id;
        $assignmentgrades->timemodified = time();
        $assignmentgrades->grader = 2;
        $assignmentgrades->grade = $gradeitemgrade;
        $return1 = $DB->update_record('assign_grades', $assignmentgrades);

        if ($DB->record_exists('assignfeedback_comments', array('assignment' => $gradeitem->iteminstance, 'grade' => $assigngrade->id))) {
            $assign_feedback = $DB->get_record('assignfeedback_comments', array('assignment' => $gradeitem->iteminstance, 'grade' => $assigngrade->id));
            print_object($assign_feedback);
            // $assign_feedback = new stdClass();
            $assign_feedback->id = $assign_feedback->id;
            $assign_feedback->assignment = $gradeitem->iteminstance;
            $assign_feedback->grade = $assigngrade->id;
            $assign_feedback->commenttext = $feedback;
            $assign_feedback->commentformat = 1;
            $return3 = $DB->update_record('assignfeedback_comments', $assign_feedback);
        } else {
            $assign_feedback = new stdClass();
            $assign_feedback->assignment = $gradeitem->iteminstance;
            $assign_feedback->grade = $assigngrade->id;
            $assign_feedback->commenttext = $feedback;
            $assign_feedback->commentformat = 1;
            $return3 = $DB->insert_record('assignfeedback_comments', $assign_feedback);
        }
    } else {
        $assignmentgrades = new stdClass();
        $assignmentgrades->assignment = $gradeitem->iteminstance;
        $assignmentgrades->userid = $userid;
        $assignmentgrades->timecreated = time();
        $assignmentgrades->timemodified = time();
        $assignmentgrades->grader = 2;
        $assignmentgrades->grade = $gradeitemgrade;
        $return1 = $DB->insert_record('assign_grades', $assignmentgrades);

        $assigngrade = $DB->get_record('assign_grades', array('assignment' => $gradeitem->iteminstance, 'userid' => $userid));

        $assign_feedback = new stdClass();
        $assign_feedback->assignment = $gradeitem->iteminstance;
        $assign_feedback->grade = $assigngrade->id;
        $assign_feedback->commenttext = $feedback;
        $assign_feedback->commentformat = 1;
        $return3 = $DB->insert_record('assignfeedback_comments', $assign_feedback);
    }
}
if ($gradeitem->itemmodule == 'quiz') {
    if ($DB->record_exists('quiz_grades', array('quiz' => $gradeitem->iteminstance, 'userid' => $userid))) {
        $quizgrade = $DB->get_record('quiz_grades', array('quiz' => $gradeitem->iteminstance, 'userid' => $userid));
        $quizitemgrades = new stdClass();
        $quizitemgrades->id = $quizgrade->id;
        $quizitemgrades->grade = $gradeitemgrade;
        $quizitemgrades->timemodified = time();
        $return1 = $DB->update_record('quiz_grades', $quizitemgrades);
    } else {
        $quizitemgrades = new stdClass();
        $quizitemgrades->quiz = $gradeitem->iteminstance;
        $quizitemgrades->userid = $userid;
        $quizitemgrades->grade = $gradeitemgrade;
        $quizitemgrades->timemodified = time();
        $return1 = $DB->insert_record('quiz_grades', $quizitemgrades);
    }
}

if ($DB->record_exists('grade_grades', array('itemid' => $gradeitem->id, 'userid' => $userid))) {
    $grade_item_grade = $DB->get_record('grade_grades', array('itemid' => $gradeitem->id, 'userid' => $userid));
    $grade_item_grade->feedback = $feedback;
    $grade_item_grade->rawgrade = $gradeitemgrade;
    $grade_item_grade->finalgrade = $gradeitemgrade;
    $grade_item_grade->timemodified = time();
    $return2 = $DB->update_record('grade_grades', $grade_item_grade);

    // Update assign feedback table
} else {
    $grade_item_grade = new stdClass();
    $grade_item_grade->itemid = $gradeitem->id;
    $grade_item_grade->userid = $userid;
    $grade_item_grade->rawgrade = $gradeitemgrade;
    $grade_item_grade->rawgrademax = $gradeitem->grademax;
    $grade_item_grade->rawgrademin = $gradeitem->grademax;
    $grade_item_grade->usermodified = 2;
    $grade_item_grade->finalgrade = $gradeitemgrade;
    $grade_item_grade->hidden = 0;
    $grade_item_grade->locked = 0;
    $grade_item_grade->loctime = 0;
    $grade_item_grade->exported = 0;
    $grade_item_grade->overridden = 0;
    $grade_item_grade->excluded = 0;
    $grade_item_grade->feedback = $feedback;
    $grade_item_grade->feedbackformat = 1;
    $grade_item_grade->timemodified = time();
    $return2 = $DB->insert_record('grade_grades', $grade_item_grade);
}
